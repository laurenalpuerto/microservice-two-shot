# Wardrobify

Team:

* Person 1 - Lauren Alpuerto (Shoes)
* Person 2 - Alexander McKelvey (Hats)

## Design



## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.

## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.


# Step-by-step: Run the Project

1. Fork the repo at https://gitlab.com/sjp19-public-resources/microservice-two-shot 
2. Open your terminal
3. Go into your projects folder
4. Clone by doing the command git clone (paste url from above)
5. Now, run the following commands:
    - docker volume create pgdata
    - docker-compose build
    - docker-compose up
6. See the application by entering this address in your browser: http://wardrobe-api:8000
7. See http://localhost:3000/shoes to view the shoes list.
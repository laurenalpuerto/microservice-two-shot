from django.urls import path

from .views import api_show_hat, api_list_hats

urlpatterns = [
    path("hats/", api_list_hats, name="api_create_hat"),
    path("locations/<int:location_vo_id>/hats/", api_list_hats, name="api_list_hats"),
    path("hats/<int:id>/", api_show_hat, name="api_show_hat")
]

import React from 'react';

class ShoeForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modelName: '',
      manufacturer: '',
      color: '',
      pictureUrl: '',
      bin: [],
    }

    this.handleChangeModel = this.handleChangeModel.bind(this);
    this.handleChangeManufacturer = this.handleChangeManufacturer.bind(this);
    this.handleChangeColor = this.handleChangeColor.bind(this);
    this.handleChangePictureUrl = this.handleChangePictureUrl.bind(this);
    this.handleChangeBin = this.handleChangeBin.bind(this);
  }

  async handleSubmit(event) {
    event.PreventDefault();
    const data = { ...this.state };
    data.model_name = data.modelName;
    data.picture_url = data.pictureUrl;
    delete data.bins;

    const shoeUrl = 'http://localhost:8080/api/shoes/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(shoeUrl, fetchConfig);
    if (response.ok) {
      const newShoe = await response.json();

      this.setState({
        modelName: '',
        manufacturer: '',
        color: '',
        pictureUrl: '',
        bin: '',
      });
      window.location.id = `http://localhost:8080/api/shoes/`
    }
  }

  async componentDidMount() { // shoes or bins?
    const url = 'http://localhost:8080/api/shoes'
    const response = await fetch(url)
    if (response.ok) {
      const data = await response.json();
      this.setState({ bins: data.bins })
      console.log(data);
    }
  }

  handleChangeModel(event) {
    const value = event.target.value;
    this.setState({ modelName: value });
  }

  handleChangeName(event) {
    const value = event.target.value;
    this.setState({ manufacturer: value });
  }

  handleChangeName(event) {
    const value = event.target.value;
    this.setState({ color: value });
  }

  handleChangeEmail(event) {
    const value = event.target.value;
    this.setState({ pictureUrl: value });
  }

  handleChangeEmail(event) {
    const value = event.target.value;
    this.setState({ bin: value });
  }

  render() {
    return (
      <div className="row">
        <div className="col col-sm-auto">
          <h1>Add New Shoe</h1>
          <form onSubmit={this.handleSubmit} id="create-shoe-form">
            <div className="form-floating mb-3">
              <input onChange={this.handleChangeModel} value={this.state.model_name} placeholder="Model Name" required type="text" name="model_name" id="model_name" className="form-control" />
              <label htmlFor="model_name">Model Name</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={this.handleChangeManufacturer} value={this.state.manufacturer} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control" />
              <label htmlFor="manufacturer">Manufacturer</label>
            </div>

            <div className="form-floating mb-3">
              <input onChange={this.handleChangeColor} value={this.state.color} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
              <label htmlFor="color">Color</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={this.handleChangePictureUrl} value={this.state.picture_url} placeholder="Picture" required type="url" name="picture_url" id="picture_url" className="form-control" />
              <label htmlFor="picture_url">Picture</label>
            </div>
            <div className="form-floating mb-3">
              <select onChange={this.handleChangeBin} value={this.state.bin} required id="bin" name="bin" className="form-select">
                <option value="">Choose bin</option>
                {this.state.bins.map(bin => {
                  return (
                    <option key={bin.id} value={bin.id}>
                      {bin.closet_name}
                    </option>
                  )
                })}
              </select>
            </div>
            <button className="btn btn-warning">Add</button>
          </form>
        </div>
      </div>
    )
  }
}

export default ShoeForm;
import React, { useState, useEffect } from "react"

function ShoeList(props) {
    const deleteShoe = async (id) => {
        fetch(`http://wardrobe:8000/shoes/${id}`, {
            method: "delete",
            headers: {
                'Content=Type': 'application/json',
            }
        })
        window.location.reload();
    }
    if (props.shoes === undefined) {
        return null
    }

    return (
            <table className="table table-striped align-middle mt-5">
                <thead>
                        <tr>
                        <th>Shoe</th>
                        <th>Color</th>
                        <th>Manufacturer</th>
                        <th>Picture</th>
                        <th>Bin</th>
                    </tr>
                </thead>
                <tbody>
                    {props.shoes.map((shoe) => {
                        console.log(shoe)
                        return (
                            <tr key={shoe.id}>
                                <td>{shoe.model_name}</td>
                                <td>{shoe.color}</td>
                                <td>{shoe.manufacturer}</td>
                                <td><img src={shoe.picture_url} className="img-thumbnail shoes" alt={shoe.manufacturer}></img></td>
                                <td>{shoe.bin}</td>
                                <td>
                                    <button type="button" value={shoe.id} onClick={() => deleteShoe(shoe.id)}>Delete</button>
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
    );
}

export default ShoeList;